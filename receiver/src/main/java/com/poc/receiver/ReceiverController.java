package com.poc.receiver;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReceiverController {
  @Value("${server.port}")
  String port;
  
  @Value("${sleep:10}")
  Long sleep;
  
  @GetMapping
  public String get() throws InterruptedException {
    Thread.sleep(sleep);
    return "instance " + port;
  }
}
