#!/bin/bash

# Sobe 4 instâncias do microsservico Receiver com tempos de resposta diferenciados
# Obs: os processos não irão gerar output para o terminal

function execute() {
  nohup ./mvnw -Dserver.port=$1 -Dsleep=$2 spring-boot:run &> /dev/null &
}

execute 8001 100
execute 8002 300
execute 8003 500
execute 8004 1000
