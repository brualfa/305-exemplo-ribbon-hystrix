#!/bin/bash

# Invoca o microsserviço Caller em um intervalo de 10 milissegundos
# Obs: use CTRL+C para encerrar o processo

while true
do
   curl localhost:8080
   echo ""
   sleep 0.1
done

