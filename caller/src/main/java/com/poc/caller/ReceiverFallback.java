package com.poc.caller;

import org.springframework.stereotype.Component;

@Component
public class ReceiverFallback implements ReceiverClient {

  @Override
  public String get() {
    return "Sou uma contigência blah";
  }
}
