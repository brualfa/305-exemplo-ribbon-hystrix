package com.poc.caller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "receiver", fallback = ReceiverFallback.class)
public interface ReceiverClient {

  @GetMapping
  public String get();
}
