package com.poc.caller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

//import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
//import com.netflix.ribbon.proxy.annotation.Hystrix;

@RestController
public class CallerController {
  @Autowired
  ReceiverClient client;
  
  Logger logger = LoggerFactory.getLogger(CallerController.class);
  
//  @HystrixCommand(fallbackMethod = "contingencia")
  @GetMapping
  public String call() {
    return client.get();
  }
  
  @GetMapping("/{id}")
  public String getId(@PathVariable String id) {
    return id;
  }
  
//  public String contingencia(Throwable e) {
//    logger.error(e.getMessage());
//    return "Eu sou a contingência";
//  }
}
